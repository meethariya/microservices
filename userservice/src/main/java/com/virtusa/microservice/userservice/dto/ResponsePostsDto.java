package com.virtusa.microservice.userservice.dto;

import lombok.Data;

/**
 * Response to send to User for posts.
 * 
 * @author MEETKIRTIBHAI
 * @since 08-Sep-2023
 */
@Data
public class ResponsePostsDto {
	private long id;
	private long userId;
	private String title;
	private String body;
}
