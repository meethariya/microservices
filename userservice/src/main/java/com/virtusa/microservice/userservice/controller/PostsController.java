package com.virtusa.microservice.userservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.virtusa.microservice.userservice.dto.ResponsePostsDto;
import com.virtusa.microservice.userservice.service.PostsService;

/**
 * Controller for Posts API.
 * 
 * @author MEETKIRTIBHAI
 * @since 08-Sep-2023
 * @see https://jsonplaceholder.typicode.com/
 */
@RestController
@RequestMapping("/posts")
public class PostsController {

	@Autowired
	PostsService postsService;

	@GetMapping
	public ResponseEntity<List<ResponsePostsDto>> getAllPosts() {
		return new ResponseEntity<>(postsService.getAllPosts(), HttpStatus.OK);
	}
}
