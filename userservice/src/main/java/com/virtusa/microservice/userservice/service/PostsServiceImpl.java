package com.virtusa.microservice.userservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.virtusa.microservice.userservice.dto.ResponsePostsDto;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

/**
 * @author MEETKIRTIBHAI
 * @since 08-Sep-2023
 */
@Service
@Transactional
@Slf4j
public class PostsServiceImpl implements PostsService {

	@Autowired
	WebClient webClient;
	
	@Value("${server.port}")
	String port;
	
	/**
	 * Returns list of all posts from jsonPlaceholder
	 * @see https://jsonplaceholder.typicode.com/posts
	 */
	@Override
	public List<ResponsePostsDto> getAllPosts() {
		log.info("POSTSSERVICE: Get all posts @PORT: " + port);
		return webClient.get()
				 .uri("https://jsonplaceholder.typicode.com/posts")
				 .retrieve()
				 .bodyToFlux(ResponsePostsDto.class)
				 .collectList()
				 .block();
	}

}
