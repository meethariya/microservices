package com.virtusa.microservice.userservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.virtusa.microservice.userservice.dto.RequestUserDto;
import com.virtusa.microservice.userservice.dto.ResponseUserDto;
import com.virtusa.microservice.userservice.service.UserService;

/**
 * Controller for User API
 * 
 * @author MEETKIRTIBHAI
 * @since 04-Sep-2023
 */
@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	UserService userService;

	/**
	 * Create new user.
	 * 
	 * @param request
	 * @return id of the user saved
	 */
	@PostMapping
	public ResponseEntity<Long> createUser(@ModelAttribute RequestUserDto request) {
		return new ResponseEntity<>(userService.createUser(request), HttpStatus.CREATED);
	}

	/**
	 * Get user by id.
	 * 
	 * @param id
	 * @return User
	 */
	@GetMapping("/{id}")
	public ResponseEntity<ResponseUserDto> getUserById(@PathVariable("id") long id) {
		return new ResponseEntity<>(userService.getUser(id), HttpStatus.OK);
	}

	/**
	 * Get user by email.
	 * 
	 * @param email
	 * @return User
	 */
	@GetMapping("/byEmail/{email}")
	public ResponseEntity<ResponseUserDto> getUserByEmail(@PathVariable("email") String email) {
		return new ResponseEntity<>(userService.getUser(email), HttpStatus.OK);
	}

	/**
	 * Update user by id.
	 * 
	 * @param id
	 * @param request
	 * @return updated user.
	 */
	@PutMapping("/{id}")
	public ResponseEntity<ResponseUserDto> updateUser(@PathVariable("id") long id,
			@ModelAttribute RequestUserDto request) {
		return new ResponseEntity<>(userService.updateUser(id, request), HttpStatus.ACCEPTED);
	}

	/**
	 * Delete user by Id.
	 * 
	 * @param id
	 */
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void deleteUserById(@PathVariable("id") long id) {
		userService.deleteUser(id);
	}
}
