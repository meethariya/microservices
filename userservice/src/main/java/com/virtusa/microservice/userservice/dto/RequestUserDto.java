package com.virtusa.microservice.userservice.dto;

import lombok.Data;

/**
 * DTO accepted from User for
 * {@link com.virtusa.microservice.userservice.model.User}
 * 
 * @author MEETKIRTIBHAI
 * @since 04-Sep-2023
 * @see {@link com.virtusa.microservice.userservice.model.User}
 */
@Data
public class RequestUserDto {
	private String email;
	private String password;
	private String phoneNo;
}
