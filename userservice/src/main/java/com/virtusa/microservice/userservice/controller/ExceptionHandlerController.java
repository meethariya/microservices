package com.virtusa.microservice.userservice.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.virtusa.microservice.userservice.exception.UserAlreadyExistException;
import com.virtusa.microservice.userservice.exception.UserNotFoundException;

/**
 * Handles all exceptions.
 * 
 * @author MEETKIRTIBHAI
 * @since 04-Sep-2023
 */
@RestControllerAdvice
public class ExceptionHandlerController {
	/**
	 * Handles error for UserNotFoundException
	 * 
	 * @param e
	 * @return Error Message response
	 */
	@ExceptionHandler(UserNotFoundException.class)
	public ResponseEntity<String> handleUserNotFoundException(UserNotFoundException e) {
		return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
	}

	/**
	 * Handles error for UserAlreadyExistException
	 * 
	 * @param e
	 * @return Error Message response
	 */
	@ExceptionHandler(UserAlreadyExistException.class)
	public ResponseEntity<String> handleUserAlreadyExistException(UserAlreadyExistException e) {
		return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
	}

}
