package com.virtusa.microservice.userservice.service;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.virtusa.microservice.userservice.dto.RequestUserDto;
import com.virtusa.microservice.userservice.dto.ResponseUserDto;
import com.virtusa.microservice.userservice.exception.UserAlreadyExistException;
import com.virtusa.microservice.userservice.exception.UserNotFoundException;
import com.virtusa.microservice.userservice.model.User;
import com.virtusa.microservice.userservice.repository.UserRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * @author MEETKIRTIBHAI
 * @since 04-Sep-2023
 */
@Service
@Transactional
@Slf4j
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	ModelMapper modelMapper;

	@Autowired
	PasswordEncoder pe;

	@Value("${server.port}")
	String port;

	/**
	 * Create new user.
	 * 
	 * @param user
	 * @return id of the saved user
	 * @throws UserNotFoundException
	 * @throws UserAlreadyExistException
	 */
	@Override
	public long createUser(RequestUserDto user) {
		log.info("USERSERVICE: Create user @PORT: " + port);
		Optional<User> findByEmail = userRepository.findByEmail(user.getEmail());
		if (findByEmail.isPresent())
			throw new UserAlreadyExistException("Email: " + user.getEmail() + " is already in use");

		User toUser = requestToUser(user);
		toUser.setPassword(pe.encode(toUser.getPassword()));

		return userRepository.save(toUser).getId();
	}

	/**
	 * Get user by id.
	 * 
	 * @param id
	 * @return user by id
	 * @throws UserNotFoundException
	 */
	@Override
	public ResponseUserDto getUser(long id) {
		log.info("USERSERVICE: Get user by id @PORT: " + port);
		Optional<User> byId = userRepository.findById(id);
		return userToResponseUser(byId.orElseThrow(() -> new UserNotFoundException("No user found with ID: " + id)));
	}

	/**
	 * Get user by email.
	 * 
	 * @param email
	 * @return user by email
	 * @throws UserNotFoundException
	 */
	@Override
	public ResponseUserDto getUser(String email) {
		log.info("USERSERVICE: Get user by email @PORT: " + port);
		Optional<User> byId = userRepository.findByEmail(email);
		return userToResponseUser(
				byId.orElseThrow(() -> new UserNotFoundException("No user found with email: " + email)));
	}

	/**
	 * Update user by id.
	 * 
	 * @param id
	 * @param request
	 * @return updated user
	 * @throws UserNotFoundException
	 */
	@Override
	public ResponseUserDto updateUser(long id, RequestUserDto request) {
		log.info("USERSERVICE: Update user @PORT: " + port);
		Optional<User> byId = userRepository.findById(id);
		User user = byId.orElseThrow(() -> new UserNotFoundException("No user found with ID: " + id));

		user.setPhoneNo(request.getPhoneNo());
		if (!user.getPassword().equals(request.getPassword()))
			user.setPassword(pe.encode(request.getPassword()));

		return userToResponseUser(userRepository.save(user));
	}

	/**
	 * Delete user by id.
	 * 
	 * @param id
	 */
	@Override
	public void deleteUser(long id) {
		log.info("USERSERVICE: Delete user by id @PORT: " + port);
		userRepository.deleteById(id);

	}

	/**
	 * Get all users.
	 * 
	 * @return List of user
	 */
	@Override
	public List<ResponseUserDto> getUser() {
		return userRepository.findAll().stream().map(this::userToResponseUser).toList();
	}

	/**
	 * Converts RequestUserDto to User model.
	 * 
	 * @param request
	 * @return user
	 */
	User requestToUser(RequestUserDto request) {
		User user = modelMapper.map(request, User.class);
		user.setEnabled(true);
		user.setRole("ROLE_user");
		return user;
	}

	/**
	 * Converts User model to ResponseUserDto
	 * 
	 * @param user
	 * @return ResponseUserDto
	 */
	ResponseUserDto userToResponseUser(User user) {
		return modelMapper.map(user, ResponseUserDto.class);
	}

	/**
	 * Converts ResponseUserDto to User model
	 * 
	 * @param response
	 * @return User
	 */
	User responseUserToUser(ResponseUserDto response) {
		return modelMapper.map(response, User.class);
	}
}
