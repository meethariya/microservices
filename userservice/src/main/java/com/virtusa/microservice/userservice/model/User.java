package com.virtusa.microservice.userservice.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;

/**
 * @author MEETKIRTIBHAI
 * @since 04-Sep-2023
 */
@Entity
@Data
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String role;
	private boolean enabled = true;
	@Column(unique = true)
	private String email;
	private String password;
	@Column(length = 10)
	private String phoneNo;
}
