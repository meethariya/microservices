package com.virtusa.microservice.userservice.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * @author MEETKIRTIBHAI
 * @since 04-Sep-2023
 */
@Slf4j
public class UserNotFoundException extends RuntimeException {

	/**
	 * Auto Generated 
	 */
	private static final long serialVersionUID = -2878182757811482993L;

	/**
	 * Default
	 */
	public UserNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Error Message.
	 * @param message
	 */
	public UserNotFoundException(String message) {
		super(message);
		log.error(message);
	}

}
