package com.virtusa.microservice.userservice.service;

import java.util.List;

import com.virtusa.microservice.userservice.dto.RequestUserDto;
import com.virtusa.microservice.userservice.dto.ResponseUserDto;

/**
 * Service layer for {@link com.virtusa.microservice.userservice.model.User}
 * 
 * @author MEETKIRTIBHAI
 * @since 04-Sep-2023
 */
public interface UserService {
	/**
	 * @param user
	 * @return id of the saved user
	 */
	public long createUser(RequestUserDto user);

	/**
	 * @param id
	 * @return user by id
	 */
	public ResponseUserDto getUser(long id);

	/**
	 * @param email
	 * @return user by email
	 */
	public ResponseUserDto getUser(String email);

	/**
	 * @param id
	 * @param request
	 * @return updated user
	 */
	public ResponseUserDto updateUser(long id, RequestUserDto request);

	/**
	 * Delete user by id.
	 * 
	 * @param id
	 */
	public void deleteUser(long id);

	/**
	 * Get all users.
	 * 
	 * @return list of user
	 */
	public List<ResponseUserDto> getUser();
}
