package com.virtusa.microservice.userservice.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * @author MEETKIRTIBHAI
 * @since 04-Sep-2023
 */
@Slf4j
public class UserAlreadyExistException extends RuntimeException {

	/**
	 * Auto Generated.
	 */
	private static final long serialVersionUID = -6536231809623494102L;

	/**
	 * Default
	 */
	public UserAlreadyExistException() {
		super();
	}

	/**
	 * Error message.
	 * 
	 * @param message
	 */
	public UserAlreadyExistException(String message) {
		super(message);
		log.error(message);
	}

}
