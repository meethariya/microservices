package com.virtusa.microservice.userservice.service;

import java.util.List;

import com.virtusa.microservice.userservice.dto.ResponsePostsDto;

/**
 * Service layer for https://jsonplaceholder.typicode.com/
 * 
 * @author MEETKIRTIBHAI
 * @since 08-Sep-2023
 */
public interface PostsService {

	public List<ResponsePostsDto> getAllPosts();

}
