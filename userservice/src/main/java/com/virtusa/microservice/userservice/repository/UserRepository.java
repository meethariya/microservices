package com.virtusa.microservice.userservice.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.virtusa.microservice.userservice.model.User;

/**
 * @author MEETKIRTIBHAI
 * @since 04-Sep-2023
 */
public interface UserRepository extends JpaRepository<User, Long> {
	public Optional<User> findByEmail(String email);
}
