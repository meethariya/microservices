package com.virtusa.microservice.userservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.virtusa.microservice.userservice.dto.ResponseUserDto;
import com.virtusa.microservice.userservice.service.UserService;

/**
 * Controller for Admin API.
 * 
 * @author MEETKIRTIBHAI
 * @since 04-Sep-2023
 */
@RestController
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	UserService userService;

	@GetMapping("/allUser")
	public ResponseEntity<List<ResponseUserDto>> getAllUsers() {
		return new ResponseEntity<>(userService.getUser(), HttpStatus.OK);
	}
}
