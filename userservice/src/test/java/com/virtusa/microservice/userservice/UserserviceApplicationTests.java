package com.virtusa.microservice.userservice;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
class UserserviceApplicationTests {

	@Autowired
	MockMvc mockMvc;
	
	@Test
	void getUserById() throws Exception {
		mockMvc.perform(get("/user/2").header("Authorization", "Basic YWJjQGdtYWlsLmNvbTphYmNk"))
			   .andExpect(status().isOk())
			   .andExpect(jsonPath("$.email").value("abc@gmail.com"));
	}
	
	@Test
	void getUserByEmail() throws Exception {
		mockMvc.perform(get("/user/byEmail/abc@gmail.com").header("Authorization", "Basic YWJjQGdtYWlsLmNvbTphYmNk"))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.id").value("2"));
	}

}
