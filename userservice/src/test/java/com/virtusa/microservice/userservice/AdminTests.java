package com.virtusa.microservice.userservice;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

/**
 * @author MEETKIRTIBHAI
 * @since 05-Sep-2023
 */
@SpringBootTest
@AutoConfigureMockMvc
class AdminTests {

	@Autowired
	MockMvc mockMvc;

	@Test
	void getAllUsers() throws Exception {
		mockMvc.perform(get("/admin/allUser").header("Authorization", "Basic YWRtaW5AZ21haWwuY29tOkFkbWluQDEyMw=="))
				.andExpect(status().isOk());
	}

}
