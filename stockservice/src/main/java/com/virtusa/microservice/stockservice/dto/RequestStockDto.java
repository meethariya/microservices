package com.virtusa.microservice.stockservice.dto;

import lombok.Data;

/**
 * DTO accepted by User for
 * {@link com.virtusa.microservice.stockservice.model.Stock}
 * 
 * @author MEETKIRTIBHAI
 * @since 03-Sep-2023
 * @see {@link com.virtusa.microservice.stockservice.model.Stock}
 */
@Data
public class RequestStockDto {
	private String bookId;
	private String lastAdded;
	private String lastRented;
	private long quantity;
}
