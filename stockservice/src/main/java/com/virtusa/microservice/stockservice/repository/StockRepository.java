package com.virtusa.microservice.stockservice.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.virtusa.microservice.stockservice.model.Stock;

/**
 * @author MEETKIRTIBHAI
 * @since 03-Sep-2023
 */
public interface StockRepository extends MongoRepository<Stock, String> {
	/**
	 * Find Stock by BookId.
	 * 
	 * @param bookId
	 * @return optional of Stock
	 */
	public Optional<Stock> findByBookId(String bookId);
}
