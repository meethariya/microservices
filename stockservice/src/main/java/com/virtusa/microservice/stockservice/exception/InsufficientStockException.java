package com.virtusa.microservice.stockservice.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * @author MEETKIRTIBHAI
 * @since 03-Sep-2023
 */
@Slf4j
public class InsufficientStockException extends RuntimeException {

	/**
	 * Auto Generated
	 */
	private static final long serialVersionUID = 1251242031064167889L;

	/**
	 * Default
	 */
	public InsufficientStockException() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Error message.
	 * 
	 * @param message
	 */
	public InsufficientStockException(String message) {
		super(message);
		log.error(message);
	}

}
