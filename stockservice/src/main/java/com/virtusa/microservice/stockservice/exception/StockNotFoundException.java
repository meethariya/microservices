package com.virtusa.microservice.stockservice.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * @author MEETKIRTIBHAI
 * @since 03-Sep-2023
 */
@Slf4j
public class StockNotFoundException extends RuntimeException {

	/**
	 * Auto Generated
	 */
	private static final long serialVersionUID = -5600552304408170813L;

	/**
	 * Default
	 */
	public StockNotFoundException() {
		super();
	}

	/**
	 * Error Message
	 * 
	 * @param message
	 */
	public StockNotFoundException(String message) {
		super(message);
		log.error(message);
	}

}
