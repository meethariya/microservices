package com.virtusa.microservice.stockservice.controller;

import java.text.ParseException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.virtusa.microservice.stockservice.exception.InsufficientStockException;
import com.virtusa.microservice.stockservice.exception.StockNotFoundException;

/**
 * Handles all Exceptions.
 * 
 * @author MEETKIRTIBHAI
 * @since 03-Sep-2023
 */
@RestControllerAdvice
public class ExceptionHandlerController {
	/**
	 * Handles error for StockNotFoundException
	 * @param e
	 * @return Error Message response
	 */
	@ExceptionHandler(StockNotFoundException.class)
	public ResponseEntity<String> handleStockNotFoundException(StockNotFoundException e){
		return new ResponseEntity<>(e.getMessage(),HttpStatus.NOT_FOUND);
	}

	/**
	 * Handles error for InsufficientStockException
	 * @param e
	 * @return Error Message response
	 */
	@ExceptionHandler(InsufficientStockException.class)
	public ResponseEntity<String> handleInsufficientStockException(InsufficientStockException e){
		return new ResponseEntity<>(e.getMessage(),HttpStatus.INSUFFICIENT_STORAGE);
	}
	
	/**
	 * Handles error for ParseException
	 * @param e
	 * @return Error Message response
	 */
	@ExceptionHandler(ParseException.class)
	public ResponseEntity<String> handleParseException(ParseException e){
		return new ResponseEntity<>("Please enter date in format 'yyyy-MM-dd'",HttpStatus.BAD_REQUEST);
	}
}
