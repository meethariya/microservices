package com.virtusa.microservice.stockservice.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

/**
 * @author MEETKIRTIBHAI
 * @since 03-Sep-2023
 */
@Document
@Data
public class Stock {
	@Id
	private String id;
	@Indexed(unique = true)
	private String bookId;
	private Date lastAdded;
	private Date lastRented;
	private long quantity;
}
