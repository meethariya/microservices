package com.virtusa.microservice.stockservice.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author MEETKIRTIBHAI
 * @since 03-Sep-2023
 */
@Configuration
public class AdditionalBeanConfig {

	/**
	 * Separate configuration file to avoid circular reference in security configuration
	 * @return BCryptPasswordEncoder
	 */	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	/**
	 * Bean for the model mapper.
	 * 
	 * @return new object for model mapper
	 */
	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

}
