package com.virtusa.microservice.stockservice.service;

import java.text.ParseException;
import java.util.List;

import com.virtusa.microservice.stockservice.dto.RequestStockDto;
import com.virtusa.microservice.stockservice.dto.ResponseStockDto;

/**
 * Service layer for {@link com.virtusa.microservice.stockservice.model.Stock}
 * 
 * @author MEETKIRTIBHAI
 * @since 03-Sep-2023
 */
public interface StockService {
	/**
	 * Create new stock.
	 * 
	 * @param request
	 * @return id of the created stock
	 * @throws ParseException for converting user's string date to Date.
	 */
	public String createStock(RequestStockDto request) throws ParseException;

	/**
	 * Get list of all the stocks.
	 * 
	 * @return list of all stocks.
	 */
	public List<ResponseStockDto> getAllStocks();

	/**
	 * Get stock by id
	 * 
	 * @param id
	 * @return Stock by id.
	 */
	public ResponseStockDto getStockById(String id);

	/**
	 * Get stock by bookId.
	 * 
	 * @param bookId
	 * @return Stock by bookId.
	 */
	public ResponseStockDto getStockByBookId(String bookId);

	/**
	 * Modify stock's quantity
	 * 
	 * @param id
	 * @param n
	 * @return modified stock.
	 */
	public ResponseStockDto modifyStockQuantity(String id, long n);

	/**
	 * Delete stock by id.
	 * 
	 * @param id
	 */
	public void deleteStockById(String id);

	/**
	 * Delete all stocks.
	 */
	public void deleteAllStock();
}
