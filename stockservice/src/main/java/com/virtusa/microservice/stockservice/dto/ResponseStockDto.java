package com.virtusa.microservice.stockservice.dto;

import java.util.Date;

import lombok.Data;

/**
 * DTO given back to User for
 * {@link com.virtusa.microservice.stockservice.model.Stock}
 * 
 * @author MEETKIRTIBHAI
 * @since 03-Sep-2023
 * @see {@link com.virtusa.microservice.stockservice.model.Stock}
 */
@Data
public class ResponseStockDto {
	private String id;
	private String bookId;
	private Date lastAdded;
	private Date lastRented;
	private long quantity;
}
