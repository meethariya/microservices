package com.virtusa.microservice.stockservice.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.virtusa.microservice.stockservice.dto.RequestStockDto;
import com.virtusa.microservice.stockservice.dto.ResponseStockDto;
import com.virtusa.microservice.stockservice.exception.InsufficientStockException;
import com.virtusa.microservice.stockservice.exception.StockNotFoundException;
import com.virtusa.microservice.stockservice.model.Stock;
import com.virtusa.microservice.stockservice.repository.StockRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * @author MEETKIRTIBHAI
 * @since 03-Sep-2023
 */
@Service
@Transactional
@Slf4j
public class StockServiceImpl implements StockService {

	@Autowired
	ModelMapper modelMapper;

	@Autowired
	StockRepository stockRepository;
	
	@Value("${server.port}")
	String port;

	/**
	 * Converts RequestStock DTO to Stock model.
	 * 
	 * @param request
	 * @return Stock
	 * @throws ParseException
	 */
	Stock requestToStock(RequestStockDto request) throws ParseException {
		Stock stock = modelMapper.map(request, Stock.class);
		Date lastAdded = new SimpleDateFormat("yyyy-MM-dd").parse(request.getLastAdded());
		Date lastRented = new SimpleDateFormat("yyyy-MM-dd").parse(request.getLastRented());
		stock.setLastAdded(lastAdded);
		stock.setLastRented(lastRented);
		return stock;
	}

	/**
	 * Converts Stock model to ResponseStock DTO
	 * 
	 * @param stock
	 * @return ResponseStockDTO
	 */
	ResponseStockDto stockToResponse(Stock stock) {
		return modelMapper.map(stock, ResponseStockDto.class);
	}

	/**
	 * Converts ResponseStock DTO to Stock model
	 * 
	 * @param stock
	 * @return ResponseStockDTO
	 */
	Stock responseToStock(ResponseStockDto stock) {
		return modelMapper.map(stock, Stock.class);
	}

	/**
	 * Create new stock.
	 * 
	 * @return Id of the created stock
	 * @throws ParseException for converting user's string date to Date.
	 */
	@Override
	public String createStock(RequestStockDto request) throws ParseException {
		log.info("STOCKSERVICE: Create stock @PORT: " + port);
		return stockRepository.save(requestToStock(request)).getId();
	}

	/**
	 * Get all stocks.
	 * 
	 * @return List of all stocks.
	 */
	@Override
	public List<ResponseStockDto> getAllStocks() {
		log.info("STOCKSERVICE: Get all stocks @PORT: " + port);
		return stockRepository.findAll().stream().map(this::stockToResponse).toList();
	}

	/**
	 * Find stock by id.
	 * 
	 * @return Stock by id.
	 * @throws StockNotFoundException
	 */
	@Override
	public ResponseStockDto getStockById(String id) {
		log.info("STOCKSERVICE: Get stock by id @PORT: " + port);
		Optional<Stock> findById = stockRepository.findById(id);
		return stockToResponse(findById.orElseThrow(() -> new StockNotFoundException("No stock found with id: " + id)));
	}

	/**
	 * Find stock by bookId.
	 * 
	 * @return Stock by bookId.
	 * @throws StockNotFoundException
	 */
	@Override
	public ResponseStockDto getStockByBookId(String bookId) {
		log.info("STOCKSERVICE: Get stock by book id @PORT: " + port);
		Optional<Stock> findByBookId = stockRepository.findByBookId(bookId);
		return stockToResponse(
				findByBookId.orElseThrow(() -> new StockNotFoundException("No stock found with bookId: " + bookId)));
	}

	/**
	 * Modifies stock quantity.
	 * 
	 * @return Stock by id.
	 * @throws StockNotFoundException
	 * @throws InsufficientStockException
	 */
	@Override
	public ResponseStockDto modifyStockQuantity(String id, long n) {
		log.info("STOCKSERVICE: Modify stock quantity @PORT: " + port);
		Stock stock = responseToStock(getStockById(id));

		if (n < 0 && n > stock.getQuantity())
			throw new InsufficientStockException(
					"Insufficient Stock. Requested: " + n + ", Available: " + stock.getQuantity());

		if (n < 0)
			stock.setLastRented(new Date());
		else
			stock.setLastAdded(new Date());

		stock.setQuantity(stock.getQuantity() + n);
		return stockToResponse(stockRepository.save(stock));
	}

	/**
	 * Delete stock by Id.
	 */
	@Override
	public void deleteStockById(String id) {
		log.info("STOCKSERVICE: Delete stock by id @PORT: " + port);
		stockRepository.deleteById(id);
	}

	/**
	 * Delete all stocks.
	 */
	@Override
	public void deleteAllStock() {
		log.info("STOCKSERVICE: Delete all stock @PORT: " + port);
		stockRepository.deleteAll();
	}
}
