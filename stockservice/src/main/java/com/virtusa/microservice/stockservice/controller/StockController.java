package com.virtusa.microservice.stockservice.controller;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.virtusa.microservice.stockservice.dto.RequestStockDto;
import com.virtusa.microservice.stockservice.dto.ResponseStockDto;
import com.virtusa.microservice.stockservice.service.StockService;

/**
 * Controller for Stock API
 * @author MEETKIRTIBHAI
 * @since 03-Sep-2023
 */
@RestController
@RequestMapping("/stock")
public class StockController {

	@Autowired
	StockService stockService;

	/**
	 * Create stock.
	 * 
	 * @param request
	 * @return Id of the stock saved
	 * @throws ParseException for converting user's string date to Date
	 */
	@PostMapping
	public ResponseEntity<String> createStock(@ModelAttribute RequestStockDto request) throws ParseException {
		return new ResponseEntity<>(stockService.createStock(request), HttpStatus.CREATED);
	}

	/**
	 * Get all stocks.
	 * 
	 * @return list of all stock
	 */
	@GetMapping
	public ResponseEntity<List<ResponseStockDto>> getAllStocks() {
		return new ResponseEntity<>(stockService.getAllStocks(), HttpStatus.OK);
	}

	/**
	 * Get stock by id.
	 * 
	 * @param id
	 * @return Stock by id
	 */
	@GetMapping("/{id}")
	public ResponseEntity<ResponseStockDto> getStockById(@PathVariable("id") String id) {
		return new ResponseEntity<>(stockService.getStockById(id), HttpStatus.OK);
	}

	/**
	 * Get stock by bookId.
	 * 
	 * @param bookId
	 * @return stock by bookId
	 */
	@GetMapping("/byBookId/{bookId}")
	public ResponseEntity<ResponseStockDto> getStockByBookId(@PathVariable("bookId") String bookId) {
		return new ResponseEntity<>(stockService.getStockByBookId(bookId), HttpStatus.OK);
	}

	/**
	 * Modify stock's quantity.
	 * 
	 * @param id
	 * @param n
	 * @return modified stock.
	 */
	@PutMapping("/{id}/{n}")
	public ResponseEntity<ResponseStockDto> modifyStockQuantity(@PathVariable("id") String id,
			@PathVariable("n") long n) {
		return new ResponseEntity<>(stockService.modifyStockQuantity(id, n), HttpStatus.ACCEPTED);
	}

	/**
	 * Delete stock by id.
	 * 
	 * @param id
	 */
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void deleteStockById(@PathVariable("id") String id) {
		stockService.deleteStockById(id);
	}

	/**
	 * Delete all stocks.
	 */
	@DeleteMapping
	@ResponseStatus(HttpStatus.OK)
	public void deleteAllStocks() {
		stockService.deleteAllStock();
	}

}
