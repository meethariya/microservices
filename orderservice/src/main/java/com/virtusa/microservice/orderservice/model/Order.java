package com.virtusa.microservice.orderservice.model;

import java.util.Date;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

/**
 * @author MEETKIRTIBHAI
 * @since 05-Sep-2023
 */
@Data
@Entity
@Table(name = "t_order")
public class Order {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private long userId;
	private String bookId;
	private Date orderDate;
	private Date returnDate;
	private double price;
}
