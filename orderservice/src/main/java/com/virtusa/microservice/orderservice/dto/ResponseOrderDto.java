package com.virtusa.microservice.orderservice.dto;

import java.util.Date;

import lombok.Data;

/**
 * DTO sent to user for
 * {@link com.virtusa.microservice.orderservice.model.Order}
 * 
 * @author MEETKIRTIBHAI
 * @since 05-Sep-2023
 * @see {@link ResponseUserDto}
 */
@Data
public class ResponseOrderDto {
	private long id;
	private long userId;
	private String bookId;
	private Date orderDate;
	private Date returnDate;
	private double price;
}
