package com.virtusa.microservice.orderservice.dto;

import java.util.Date;

import lombok.Data;

/**
 * DTO sent to user for
 * {@link com.virtusa.microservice.orderservice.model.Order}
 * 
 * @author MEETKIRTIBHAI
 * @since 07-Sep-2023
 * @see {@link com.virtusa.microservice.orderservice.dto.ResponseBookDto}
 */
@Data
public class ResponseOrderWithBookDto {
	private long id;
	private long userId;
	private ResponseBookDto book;
	private Date orderDate;
	private Date returnDate;
	private double price;
}
