package com.virtusa.microservice.orderservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.virtusa.microservice.orderservice.model.Order;

/**
 * @author MEETKIRTIBHAI
 * @since 05-Sep-2023
 */
public interface OrderRepository extends JpaRepository<Order, Long> {

}
