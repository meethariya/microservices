package com.virtusa.microservice.orderservice.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.virtusa.microservice.orderservice.exception.OrderNotFoundException;

/**
 * Handles all Exceptions.
 * 
 * @author MEETKIRTIBHAI
 * @since 03-Sep-2023
 */
@RestControllerAdvice
public class ExceptionHandlerController {

	/**
	 * Handles error for OrderNotFoundException
	 * 
	 * @param e
	 * @return Error Message response
	 */
	@ExceptionHandler(OrderNotFoundException.class)
	public ResponseEntity<String> handleOrderNotFoundException(OrderNotFoundException e) {
		return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
	}

}
