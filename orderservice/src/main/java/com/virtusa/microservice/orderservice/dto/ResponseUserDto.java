package com.virtusa.microservice.orderservice.dto;

import lombok.Data;

/**
 * DTO sent to user for {@link com.virtusa.microservice.userservice.model.User}
 * 
 * @author MEETKIRTIBHAI
 * @since 04-Sep-2023
 * @see {@link com.virtusa.microservice.userservice.model.User}
 */
@Data
public class ResponseUserDto {

	private long id;
	private String role;
	private boolean enabled = true;
	private String email;
	private String phoneNo;
}
