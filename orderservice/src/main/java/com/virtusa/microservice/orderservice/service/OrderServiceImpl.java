package com.virtusa.microservice.orderservice.service;

import java.time.YearMonth;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.virtusa.microservice.orderservice.dto.ResponseBookDto;
import com.virtusa.microservice.orderservice.dto.ResponseOrderDto;
import com.virtusa.microservice.orderservice.dto.ResponseOrderWithBookDto;
import com.virtusa.microservice.orderservice.dto.ResponseStockDto;
import com.virtusa.microservice.orderservice.dto.ResponseUserDto;
import com.virtusa.microservice.orderservice.exception.OrderNotFoundException;
import com.virtusa.microservice.orderservice.model.Order;
import com.virtusa.microservice.orderservice.repository.OrderRepository;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

/**
 * @author MEETKIRTIBHAI
 * @since 05-Sep-2023
 */
@Service
@Transactional
@Slf4j
public class OrderServiceImpl implements OrderService {

	@Autowired
	OrderRepository orderRepository;

	@Autowired
	ModelMapper modelMapper;

	@Autowired
	WebClient.Builder webClient;

	@Value("${server.port}")
	String port;

	/**
	 * Get all orders.
	 * 
	 * @return List of orders.
	 */
	@Override
	public List<ResponseOrderDto> getOrder() {
		log.info("ORDERSERVICE: Get all orders @PORT: " + port);
		return orderRepository.findAll().stream().map(this::orderToResponseOrder).toList();
	}

	/**
	 * Get order by id.
	 * 
	 * @return Order by id.
	 * @throws OrderNotFoundException
	 */
	@Override
	public ResponseOrderDto getOrder(long id) {
		log.info("ORDERSERVICE: Get order by id @PORT: " + port);
		Optional<Order> findById = orderRepository.findById(id);
		return orderToResponseOrder(
				findById.orElseThrow(() -> new OrderNotFoundException("No order found with id: " + id)));
	}

	/**
	 * Place an order.
	 * 
	 * @return id of the order created.
	 */
	@Override
	public long createOrder(String bookId, String email, String password) {
		log.info("ORDERSERVICE: Create order @PORT: " + port);
		Order order = new Order();

		String header = "Basic " + btoa(email + ":" + password);

		// Call user API, Find by email and set user id
		ResponseUserDto userDto = webClient.build().get().uri("http://userservice/user/byEmail/" + email)
				.header("Authorization", header).retrieve().bodyToMono(ResponseUserDto.class).block();
		order.setUserId(userDto.getId());

		// Call stock API, Deduct 1 stock from the given bookID
		modifyStockWithBookId(header, bookId, -1);

		order.setBookId(bookId);

		order.setOrderDate(new Date());
		return orderRepository.save(order).getId();
	}

	/**
	 * Return an order.
	 * 
	 * @return order
	 */
	@Override
	public ResponseOrderDto returnOrder(long id, String email, String password) {
		log.info("ORDERSERVICE: Return order @PORT: " + port);
		Order order = responseOrderToOrder(getOrder(id));
		order.setReturnDate(new Date());

		long months = getMonthsDifference(order.getOrderDate(), order.getReturnDate());

		// Call Book API, find book by Price and calculate price based on date diff
		String header = "Basic " + btoa(email + ":" + password);
		double price = webClient.build().get().uri("http://bookservice/book/" + order.getBookId()).retrieve()
				.bodyToMono(ResponseBookDto.class).block().getPrice();
		if (months <= 1)
			price = price * 0.5;
		else if (months <= 3)
			price = price * 0.75;
		else
			price = price * 0.9;
		order.setPrice(price);

		// Call stock API, Add 1 stock to the order's bookId
		modifyStockWithBookId(header, order.getBookId(), 1);

		return orderToResponseOrder(orderRepository.save(order));
	}

	/**
	 * Delete an order by id.
	 */
	@Override
	public void deleteOrder(long id) {
		log.info("ORDERSERVICE: Delete order by id @PORT: " + port);
		orderRepository.deleteById(id);
	}

	/**
	 * Get order by id.
	 * 
	 * @param id
	 * @return Order by Id
	 */
	@Override
	public ResponseOrderWithBookDto getOrderWithBook(long id) {
		log.info("ORDERSERVICE: Get order with book @PORT: " + port);
		Optional<Order> findById = orderRepository.findById(id);
		Order order = findById.orElseThrow(() -> new OrderNotFoundException("No order found with id: " + id));
		ResponseBookDto book = webClient.build().get().uri("http://bookservice/book/" + order.getBookId()).retrieve()
				.bodyToMono(ResponseBookDto.class).block();
		ResponseOrderWithBookDto orderToResponseWithBook = orderToResponseWithBook(order);
		orderToResponseWithBook.setBook(book);
		return orderToResponseWithBook;
	}

	/**
	 * Converts Order model to ResponseOrderDto.
	 * 
	 * @param order
	 * @return ResponseOrderDto
	 */
	ResponseOrderDto orderToResponseOrder(Order order) {
		return modelMapper.map(order, ResponseOrderDto.class);
	}

	/**
	 * Converts ResponseOrderDto to Order.
	 * 
	 * @param response
	 * @return Order
	 */
	Order responseOrderToOrder(ResponseOrderDto response) {
		return modelMapper.map(response, Order.class);
	}

	/**
	 * Calculate difference between 2 dates in months.
	 * 
	 * @param date1
	 * @param date2
	 * @return difference in months
	 */
	private static final long getMonthsDifference(Date date1, Date date2) {
		YearMonth m1 = YearMonth.from(date1.toInstant().atZone(ZoneOffset.UTC));
		YearMonth m2 = YearMonth.from(date2.toInstant().atZone(ZoneOffset.UTC));

		return m1.until(m2, ChronoUnit.MONTHS) + 1;
	}

	/**
	 * Encode Input to BTOA.
	 * 
	 * @param input
	 * @return Base64 Encoded string
	 */
	private static final String btoa(String input) {
		return Base64.getEncoder().encodeToString(input.getBytes());
	}

	/**
	 * Finds stock using book id and, adds/removes n stock to/from it.
	 * 
	 * @param header
	 * @param bookId
	 * @param n
	 */
	private final void modifyStockWithBookId(String header, String bookId, long n) {
		String stockId = webClient.build().get().uri("http://stockservice/stock/byBookId/" + bookId)
				.header("Authorization", header).retrieve().bodyToMono(ResponseStockDto.class).block().getId();

		webClient.build().put().uri("http://stockservice/stock/" + stockId + "/" + n).header("Authorization", header)
				.retrieve().bodyToMono(ResponseStockDto.class).block();
	}

	/**
	 * Converts Order to ResponseOrderWithBookDto.
	 * 
	 * @param order
	 * @return OrderWithBook
	 */
	ResponseOrderWithBookDto orderToResponseWithBook(Order order) {
		return modelMapper.map(order, ResponseOrderWithBookDto.class);
	}
}
