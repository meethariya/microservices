package com.virtusa.microservice.orderservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.virtusa.microservice.orderservice.dto.CreateOrderDto;
import com.virtusa.microservice.orderservice.dto.ResponseOrderDto;
import com.virtusa.microservice.orderservice.dto.ResponseOrderWithBookDto;
import com.virtusa.microservice.orderservice.service.OrderService;

/**
 * Controller for Order API
 * 
 * @author MEETKIRTIBHAI
 * @since 05-Sep-2023
 */
@RestController
@RequestMapping("/order")
public class OrderController {

	@Autowired
	OrderService orderService;

	/**
	 * Get all Orders.
	 * 
	 * @return List of order.
	 */
	@GetMapping
	public ResponseEntity<List<ResponseOrderDto>> getAllOrders() {
		return new ResponseEntity<>(orderService.getOrder(), HttpStatus.OK);
	}

	/**
	 * Get order by id.
	 * 
	 * @param id
	 * @return Order
	 */
	@GetMapping("/{id}")
	public ResponseEntity<ResponseOrderDto> getOrderById(@PathVariable("id") long id) {
		return new ResponseEntity<>(orderService.getOrder(id), HttpStatus.OK);
	}

	/**
	 * Create an order.
	 * 
	 * @param createOrderDto
	 * @param auth
	 * @return Id of the order created.
	 */
	@PostMapping
	public ResponseEntity<Long> placeOrder(@ModelAttribute CreateOrderDto createOrderDto, Authentication auth) {
		return new ResponseEntity<>(
				orderService.createOrder(createOrderDto.getBookId(), auth.getName(), auth.getCredentials().toString()),
				HttpStatus.CREATED);
	}

	/**
	 * Return an order(return a book).
	 * 
	 * @param id orderId
	 * @return updated order.
	 */
	@PutMapping("/{id}")
	public ResponseEntity<ResponseOrderDto> returnBook(@PathVariable("id") long id, Authentication auth) {
		return new ResponseEntity<>(orderService.returnOrder(id, auth.getName(), auth.getCredentials().toString()),
				HttpStatus.ACCEPTED);
	}

	/**
	 * Delete an order.
	 * 
	 * @param id
	 */
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void deleteOrder(@PathVariable("id") long id) {
		orderService.deleteOrder(id);
	}

	/**
	 * Return Order with book.
	 * 
	 * @param id
	 * @return ResponseOrderWithBookDto
	 */
	@GetMapping("/orderWithBook/{id}")
	public ResponseEntity<ResponseOrderWithBookDto> orderWithBook(@PathVariable("id") long id) {
		return new ResponseEntity<>(orderService.getOrderWithBook(id), HttpStatus.OK);
	}
}
