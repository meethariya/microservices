package com.virtusa.microservice.orderservice.dto;

import lombok.Data;

/**
 * @author MEETKIRTIBHAI
 * @since 03-Sep-2023
 */
@Data
public class Category {
	private String id;
	private String name;
}
