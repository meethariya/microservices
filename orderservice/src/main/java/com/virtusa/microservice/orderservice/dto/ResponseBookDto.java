package com.virtusa.microservice.orderservice.dto;

import lombok.Data;

/**
 * DTO given back to User for
 * {@link com.virtusa.microservice.bookservice.model.Book}
 * 
 * @author MEETKIRTIBHAI
 * @since 03-Sep-2023
 * @see {@link com.virtusa.microservice.bookservice.model.Book}
 * @see {@link com.virtusa.microservice.bookservice.model.Category}
 */
@Data
public class ResponseBookDto {
	private String id;
	private String name;
	private String author;
	private String coverPageUrl;
	private double price;
	private Category category;
}
