package com.virtusa.microservice.orderservice.service;

import java.util.List;

import com.virtusa.microservice.orderservice.dto.ResponseOrderDto;
import com.virtusa.microservice.orderservice.dto.ResponseOrderWithBookDto;

/**
 * Service layer for {@link com.virtusa.microservice.orderservice.model.Order}
 * 
 * @author MEETKIRTIBHAI
 * @since 05-Sep-2023
 */
public interface OrderService {

	/**
	 * All order.
	 * 
	 * @return List of all orders
	 */
	public List<ResponseOrderDto> getOrder();

	/**
	 * Get order by id.
	 * 
	 * @param id
	 * @return Order by Id
	 */
	public ResponseOrderDto getOrder(long id);

	/**
	 * Create order.
	 * 
	 * @param bookId
	 * @param email
	 * @param password
	 * @return Id of the order created.
	 */
	public long createOrder(String bookId, String email, String password);

	/**
	 * Return back a book.
	 * 
	 * @param id
	 * @return updated Order
	 */
	public ResponseOrderDto returnOrder(long id, String email, String password);

	/**
	 * Delete an order by id.
	 * 
	 * @param id
	 */
	public void deleteOrder(long id);

	/**
	 * Get order along with book.
	 * 
	 * @param id
	 * @return Order
	 */
	public ResponseOrderWithBookDto getOrderWithBook(long id);
}
