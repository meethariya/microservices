package com.virtusa.microservice.orderservice.dto;

import lombok.Data;

/**
 * DTO to take book id from user to place order.
 * 
 * @author MEETKIRTIBHAI
 * @since 05-Sep-2023
 */
@Data
public class CreateOrderDto {
	private String bookId;
}
