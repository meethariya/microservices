package com.virtusa.microservice.orderservice.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * @author MEETKIRTIBHAI
 * @since 05-Sep-2023
 */
@Slf4j
public class OrderNotFoundException extends RuntimeException {

	/**
	 * Auto Generated.
	 */
	private static final long serialVersionUID = -6646570800165875174L;

	/**
	 * Default.s
	 */
	public OrderNotFoundException() {
		super();
	}

	/**
	 * Error message.
	 * 
	 * @param message
	 */
	public OrderNotFoundException(String message) {
		super(message);
		log.error(message);
	}

}
