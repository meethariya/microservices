package com.virtusa.microservice.orderservice;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
class OrderserviceApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@Test
	void getAllOrders() throws Exception {
		mockMvc.perform(get("/order").header("Authorization", "Basic YWRtaW5AZ21haWwuY29tOkFkbWluQDEyMw==")).andExpect(status().isOk());
	}
	
	@Test
	void getOrderById() throws Exception {
		mockMvc.perform(get("/order/1").header("Authorization", "Basic YWRtaW5AZ21haWwuY29tOkFkbWluQDEyMw==")).andExpect(status().isNotFound());
	}

}
