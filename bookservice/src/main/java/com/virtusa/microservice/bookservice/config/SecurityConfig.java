package com.virtusa.microservice.bookservice.config;

import static org.springframework.security.config.Customizer.withDefaults;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author MEETKIRTIBHAI
 * @since 03-Sep-2023
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig implements WebMvcConfigurer {

	@Autowired
	DataSource dataSource;
	
	/**
	 * Security configuration for all URLs, Login, Logout, CSRF and basic HTTP.
	 * 
	 * @param http
	 * @return Security FilterChain
	 * @throws Exception
	 */
	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {

		http.authorizeHttpRequests((authorizeHttpRequests) -> authorizeHttpRequests
				.requestMatchers(HttpMethod.GET, "/book/**").permitAll()
				.requestMatchers(HttpMethod.GET, "/category/**").permitAll()
				.requestMatchers("/book/**").hasRole("admin")
				.requestMatchers("/category/**").hasRole("admin"))
				.httpBasic(withDefaults()).cors(withDefaults())
				.formLogin(((formLogin) -> formLogin.usernameParameter("email").permitAll()))
				.logout(((logout) -> logout.permitAll())).csrf((csrf) -> csrf.disable());

		return http.build();
	}

	/**
	 * Sets query to get User for login and role verification. Password encoder is
	 * autowired.<br>
	 * {@link AdditionalBeanConfig#passwordEncoder()}
	 * 
	 * @param auth
	 * @param pe
	 * @throws Exception
	 */
	@Autowired
	public void configure(AuthenticationManagerBuilder auth, PasswordEncoder pe) throws Exception {
		auth.jdbcAuthentication().usersByUsernameQuery("SELECT email, password, enabled FROM user WHERE email = ?")
				.authoritiesByUsernameQuery("select email, role FROM user where email=?").dataSource(dataSource)
				.passwordEncoder(pe);
		auth.eraseCredentials(false);
	}

}
