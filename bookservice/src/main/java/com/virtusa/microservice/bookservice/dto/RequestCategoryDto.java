package com.virtusa.microservice.bookservice.dto;

import lombok.Data;

/**
 * DTO given from User for
 * {@link com.virtusa.microservice.bookservice.model.Category}
 * 
 * @author MEETKIRTIBHAI
 * @since 05-Sep-2023
 */
@Data
public class RequestCategoryDto {
	private String name;
}
