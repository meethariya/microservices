package com.virtusa.microservice.bookservice.service;

import java.util.List;

import com.virtusa.microservice.bookservice.dto.RequestCategoryDto;
import com.virtusa.microservice.bookservice.dto.ResponseCategoryDto;

/**
 * Service layer for {@link com.virtusa.microservice.bookservice.model.Category}
 * 
 * @author MEETKIRTIBHAI
 * @since 03-Sep-2023
 */
public interface CategoryService {

	/**
	 * @param name
	 * @return Id of the save category.
	 */
	public String addCategory(RequestCategoryDto name);

	/**
	 * @return List of all the categories.
	 */
	public List<ResponseCategoryDto> getAllCategory();

	/**
	 * @param id
	 * @return Category by Id.
	 */
	public ResponseCategoryDto getCategoryById(String id);
	
	/**
	 * @param name
	 * @return Category by name.
	 */
	public ResponseCategoryDto getCategoryByName(String name);

	/**
	 * @param id
	 * @param name
	 * @return Updated Category
	 */
	public ResponseCategoryDto updateCategory(String id, RequestCategoryDto name);

	/**
	 * Delete category by Id.
	 * 
	 * @param id
	 */
	public void deleteCategoryById(String id);

	/**
	 * Delete all categories.
	 */
	public void deleteAllCategory();
}
