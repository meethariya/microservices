package com.virtusa.microservice.bookservice.service;

import java.util.List;

import com.virtusa.microservice.bookservice.dto.RequestBookDto;
import com.virtusa.microservice.bookservice.dto.ResponseBookDto;

/**
 * Service layer for {@link com.virtusa.microservice.bookservice.model.Book}
 * 
 * @author MEETKIRTIBHAI
 * @since 03-Sep-2023
 */
public interface BookService {
	/**
	 * @param requestBook
	 * @param email
	 * @param password
	 * @return Id of the book saved
	 */
	public String addBook(RequestBookDto requestBook, String email, String password);

	/**
	 * @return List of all the books.
	 */
	public List<ResponseBookDto> getAllBooks();

	/**
	 * @param id
	 * @return Book by id
	 */
	public ResponseBookDto getBookById(String id);

	/**
	 * @param id
	 * @param requestBook
	 * @return Updated book
	 */
	public ResponseBookDto updateBook(String id, RequestBookDto requestBook);

	/**
	 * Delete book by id
	 * 
	 * @param id
	 */
	public void deleteBookById(String id, String email, String password);

	/**
	 * Delete all books
	 * 
	 * @param password
	 * @param email
	 */
	public void deleteAllBooks(String email, String password);
}
