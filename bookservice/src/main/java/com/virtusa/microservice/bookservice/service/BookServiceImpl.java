package com.virtusa.microservice.bookservice.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.virtusa.microservice.bookservice.dto.RequestBookDto;
import com.virtusa.microservice.bookservice.dto.RequestCategoryDto;
import com.virtusa.microservice.bookservice.dto.RequestStockDto;
import com.virtusa.microservice.bookservice.dto.ResponseBookDto;
import com.virtusa.microservice.bookservice.dto.ResponseCategoryDto;
import com.virtusa.microservice.bookservice.exception.BookNotFoundException;
import com.virtusa.microservice.bookservice.model.Book;
import com.virtusa.microservice.bookservice.model.Category;
import com.virtusa.microservice.bookservice.openfeign.StockApiClient;
import com.virtusa.microservice.bookservice.repository.BookRepository;
import com.virtusa.microservice.bookservice.repository.CategoryRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * @author MEETKIRTIBHAI
 * @since 03-Sep-2023
 */
@Service
@Transactional
@Slf4j
public class BookServiceImpl implements BookService {

	@Autowired
	BookRepository bookRepository;

	@Autowired
	CategoryRepository categoryRepository;

	@Autowired
	CategoryServiceImpl categoryService;

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	StockApiClient stockApiClient;

	@Autowired
	ModelMapper modelMapper;

	@Value("${server.port}")
	String port;

	/**
	 * Create new Book.
	 * 
	 * @return id of book saved.
	 */
	@Override
	public String addBook(RequestBookDto requestBook, String email, String password) {
		log.info("BOOKSERVICE: Create book @PORT: " + port);
		String bookId = bookRepository.save(requestToBook(requestBook)).getId();

		// Add Stock data as well when adding book.
		RequestStockDto requestStockDto = new RequestStockDto();
		requestStockDto.setBookId(bookId);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		requestStockDto.setLastAdded(df.format(new Date()));
		requestStockDto.setLastRented(df.format(new Date()));
		requestStockDto.setQuantity(0);

		String header = "Basic " + btoa(email + ":" + password);
		stockApiClient.addStock(header, requestStockDto);

		return bookId;
	}

	/**
	 * @return list of all books.
	 */
	@Override
	public List<ResponseBookDto> getAllBooks() {
		log.info("BOOKSERVICE: Get all books @PORT: " + port);
		return bookRepository.findAll().stream().map(this::bookToResponse).toList();
	}

	/**
	 * @return book by id.
	 * @throws BookNotFoundException
	 */
	@Override
	public ResponseBookDto getBookById(String id) {
		log.info("BOOKSERVICE: Get book by id @PORT: " + port);
		Optional<Book> byId = bookRepository.findById(id);
		return bookToResponse(byId.orElseThrow(() -> new BookNotFoundException("No book found with id: " + id)));
	}

	/**
	 * @return updated book.
	 */
	@Override
	public ResponseBookDto updateBook(String id, RequestBookDto requestBook) {
		log.info("BOOKSERVICE: Update book by id @PORT: " + port);
		Book book = requestToBook(requestBook);
		book.setId(id);
		return bookToResponse(bookRepository.save(book));
	}

	/**
	 * Delete book by id.
	 */
	@Override
	public void deleteBookById(String id, String email, String password) {
		log.info("BOOKSERVICE: Delete book by id @PORT: " + port);
		bookRepository.deleteById(id);

		// Delete stock of the same book. USING REST TEMPLATE

		String header = "Basic " + btoa(email + ":" + password);
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		headers.add("Authorization", header);
		HttpEntity<?> entity = new HttpEntity<Object>(headers);
		ResponseBookDto responseBookDto = restTemplate
				.exchange("http://stockservice/stock/byBookId/" + id, HttpMethod.GET, entity, ResponseBookDto.class)
				.getBody();
		log.info(responseBookDto.getId());
		restTemplate.exchange("http://stockservice/stock/" + responseBookDto.getId(), HttpMethod.DELETE, entity,
				Void.class);
	}

	/**
	 * Delete all books.
	 */
	@Override
	public void deleteAllBooks(String email, String password) {
		log.info("BOOKSERVICE: Delete all books @PORT: " + port);
		bookRepository.deleteAll();
		// Delete all the stocks as well.
		String header = "Basic " + btoa(email + ":" + password);
		stockApiClient.deleteAllStocks(header);
	}

	/**
	 * Converts request book DTO to book model.
	 * 
	 * @param requestBook
	 * @return book
	 */
	Book requestToBook(RequestBookDto requestBook) {
		Category category;
		// find category if found add that, else create new.
		Optional<Category> findByName = categoryRepository.findByName(requestBook.getCategory());
		if (findByName.isPresent()) {
			category = findByName.get();
		} else {
			RequestCategoryDto temp = new RequestCategoryDto();
			temp.setName(requestBook.getCategory());

			String id = categoryService.addCategory(temp);
			ResponseCategoryDto tempCategory = categoryService.getCategoryById(id);

			category = categoryService.responseToCategory(tempCategory);
		}
		Book book = modelMapper.map(requestBook, Book.class);
		book.setCategory(category);
		return book;
	}

	/**
	 * Converts book model to response book DTO.
	 * 
	 * @param book
	 * @return ResponseBookDto
	 */
	ResponseBookDto bookToResponse(Book book) {
		return modelMapper.map(book, ResponseBookDto.class);
	}

	/**
	 * Encode Input to BTOA.
	 * 
	 * @param input
	 * @return Base64 Encoded string
	 */
	private static final String btoa(String input) {
		return Base64.getEncoder().encodeToString(input.getBytes());
	}
}
