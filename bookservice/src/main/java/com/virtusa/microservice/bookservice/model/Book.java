package com.virtusa.microservice.bookservice.model;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

/**
 * @author MEETKIRTIBHAI
 * @since 03-Sep-2023
 * @see Category
 */
@Document
@Data
public class Book {
	private String id;
	private String name;
	private String author;
	private String coverPageUrl;
	private double price;
	private Category category;
}
