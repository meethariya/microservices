package com.virtusa.microservice.bookservice.service;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.virtusa.microservice.bookservice.dto.RequestCategoryDto;
import com.virtusa.microservice.bookservice.dto.ResponseCategoryDto;
import com.virtusa.microservice.bookservice.exception.CategoryNotFoundException;
import com.virtusa.microservice.bookservice.model.Category;
import com.virtusa.microservice.bookservice.repository.CategoryRepository;

/**
 * @author MEETKIRTIBHAI
 * @since 03-Sep-2023
 */
@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	CategoryRepository categoryRepository;

	@Autowired
	ModelMapper modelMapper;

	/**
	 * Creates new category.
	 */
	@Override
	public String addCategory(RequestCategoryDto request) {
		String name = requestToCategory(request).getName();
		Optional<Category> byName = categoryRepository.findByName(name);
		if (byName.isPresent())
			return byName.get().getId();
		else {
			Category category = new Category();
			category.setName(name);
			return categoryRepository.save(category).getId();
		}
	}

	/**
	 * @return all categories.
	 */
	@Override
	public List<ResponseCategoryDto> getAllCategory() {
		return categoryRepository.findAll().stream().map(this::categoryToResponse).toList();
	}

	/**
	 * @return Category by id
	 * @throws CategoryNotFoundException
	 */
	@Override
	public ResponseCategoryDto getCategoryById(String id) {
		Optional<Category> byId = categoryRepository.findById(id);
		return categoryToResponse(
				byId.orElseThrow(() -> new CategoryNotFoundException("No Category found with id: " + id)));
	}

	/**
	 * @return Category by id.
	 * @throws CategoryNotFoundException
	 */
	@Override
	public ResponseCategoryDto getCategoryByName(String name) {
		Optional<Category> byId = categoryRepository.findByName(name);
		return categoryToResponse(
				byId.orElseThrow(() -> new CategoryNotFoundException("No Category found with name: " + name)));
	}

	/**
	 * Updates category.
	 */
	@Override
	public ResponseCategoryDto updateCategory(String id, RequestCategoryDto name) {
		Category category = responseToCategory(getCategoryById(id));
		category.setName(name.getName());
		return categoryToResponse(categoryRepository.save(category));
	}

	/**
	 * Delete category by id.
	 */
	@Override
	public void deleteCategoryById(String id) {
		categoryRepository.deleteById(id);
	}

	/**
	 * Delete all Categories.
	 */
	@Override
	public void deleteAllCategory() {
		categoryRepository.deleteAll();
	}

	/**
	 * Converts ResponseCategory DTO to Category model.
	 * 
	 * @param resCategory
	 * @return Category
	 */
	Category responseToCategory(ResponseCategoryDto resCategory) {
		return modelMapper.map(resCategory, Category.class);
	}

	/**
	 * Converts RequestCategoryDto to Category model.
	 * 
	 * @param request
	 * @return Category
	 */
	Category requestToCategory(RequestCategoryDto request) {
		return modelMapper.map(request, Category.class);
	}

	/**
	 * Converts Category to ResponseCategory DTO.
	 * 
	 * @param category
	 * @return ResponseCategory
	 */
	ResponseCategoryDto categoryToResponse(Category category) {
		return modelMapper.map(category, ResponseCategoryDto.class);
	}
}
