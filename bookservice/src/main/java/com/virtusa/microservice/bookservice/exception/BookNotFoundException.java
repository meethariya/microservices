package com.virtusa.microservice.bookservice.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * @author MEETKIRTIBHAI
 * @since 03-Sep-2023
 */
@Slf4j
public class BookNotFoundException extends RuntimeException {

	/**
	 * Auto generated.
	 */
	private static final long serialVersionUID = 6285521562239794702L;

	/**
	 * Default
	 */
	public BookNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Error message.
	 * 
	 * @param message
	 */
	public BookNotFoundException(String message) {
		super(message);
		log.error(message);
	}

}
