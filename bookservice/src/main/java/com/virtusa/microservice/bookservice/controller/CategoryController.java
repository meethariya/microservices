package com.virtusa.microservice.bookservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.virtusa.microservice.bookservice.dto.RequestCategoryDto;
import com.virtusa.microservice.bookservice.dto.ResponseCategoryDto;
import com.virtusa.microservice.bookservice.service.CategoryService;

/**
 * Controller for Category API
 * 
 * @author MEETKIRTIBHAI
 * @since 03-Sep-2023
 */
@RestController
@RequestMapping("/category")
public class CategoryController {

	@Autowired
	CategoryService categoryService;

	/**
	 * Create new category.
	 * 
	 * @param name
	 * @return Id of saved category
	 */
	@PostMapping
	public ResponseEntity<String> addCategory(@ModelAttribute RequestCategoryDto name) {
		return new ResponseEntity<>(categoryService.addCategory(name), HttpStatus.CREATED);
	}

	/**
	 * Get all categories.
	 * 
	 * @return List of all categories.
	 */
	@GetMapping
	public ResponseEntity<List<ResponseCategoryDto>> getAllCategories() {
		return new ResponseEntity<>(categoryService.getAllCategory(), HttpStatus.OK);
	}

	/**
	 * Find category by id.
	 * 
	 * @param id
	 * @return Category by ID
	 */
	@GetMapping("/{id}")
	public ResponseEntity<ResponseCategoryDto> getCategoryById(@PathVariable("id") String id) {
		return new ResponseEntity<>(categoryService.getCategoryById(id), HttpStatus.OK);
	}

	/**
	 * Find category by name.
	 * 
	 * @param name
	 * @return Category by name
	 */
	@GetMapping("/byName/{name}")
	public ResponseEntity<ResponseCategoryDto> getCategoryByName(@PathVariable("name") String name) {
		return new ResponseEntity<>(categoryService.getCategoryByName(name), HttpStatus.OK);
	}

	/**
	 * Update category.
	 * 
	 * @param id
	 * @param name
	 * @return updated category
	 */
	@PutMapping("/{id}")
	public ResponseEntity<ResponseCategoryDto> updateCategory(@PathVariable("id") String id,
			@ModelAttribute RequestCategoryDto name) {
		// TODO: Bugged
		return new ResponseEntity<>(categoryService.updateCategory(id, name), HttpStatus.ACCEPTED);
	}

	/**
	 * Delete category by Id.
	 * 
	 * @param id
	 */
	@DeleteMapping("/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public void deleteCategoryById(@PathVariable("id") String id) {
		categoryService.deleteCategoryById(id);
	}

	/**
	 * Delete all categories.
	 */
	@DeleteMapping
	@ResponseStatus(code = HttpStatus.OK)
	public void deleteAllCategories() {
		categoryService.deleteAllCategory();
	}
}
