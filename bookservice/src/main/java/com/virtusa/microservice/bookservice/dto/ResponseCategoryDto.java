package com.virtusa.microservice.bookservice.dto;

import lombok.Data;

/**
 * DTO given back to User for
 * {@link com.virtusa.microservice.bookservice.model.Category}
 * 
 * @author MEETKIRTIBHAI
 * @since 03-Sep-2023
 * @see {@link com.virtusa.microservice.bookservice.model.Category}
 */
@Data
public class ResponseCategoryDto {
	private String id;
	private String name;
}
