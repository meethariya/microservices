package com.virtusa.microservice.bookservice.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * @author MEETKIRTIBHAI
 * @since 03-Sep-2023
 */
@Slf4j
public class CategoryNotFoundException extends RuntimeException {

	/**
	 * Auto Generated
	 */
	private static final long serialVersionUID = -1179363419380825787L;

	/**
	 * Default
	 */
	public CategoryNotFoundException() {
		super();
	}

	/**
	 * Error message.
	 * 
	 * @param message
	 */
	public CategoryNotFoundException(String message) {
		super(message);
		log.error(message);
	}

}
