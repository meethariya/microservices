package com.virtusa.microservice.bookservice.dto;

import lombok.Data;

/**
 * DTO accepted by User for
 * {@link com.virtusa.microservice.bookservice.model.Book}
 * 
 * @author MEETKIRTIBHAI
 * @since 03-Sep-2023
 * @see {@link com.virtusa.microservice.bookservice.model.Book}
 */
@Data
public class RequestBookDto {
	private String name;
	private String author;
	private String coverPageUrl;
	private double price;
	private String category;
}
