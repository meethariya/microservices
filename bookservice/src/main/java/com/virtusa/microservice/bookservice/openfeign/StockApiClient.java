package com.virtusa.microservice.bookservice.openfeign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;

import com.virtusa.microservice.bookservice.dto.RequestStockDto;

/**
 * Stock API using OpenFeign.
 * 
 * @author MEETKIRTIBHAI
 * @since 07-Sep-2023
 */
@FeignClient(name = "stockservice")
public interface StockApiClient {

	/**
	 * Create stock.
	 * 
	 * @param authorization
	 * @param stock
	 * @return stock ID
	 */
	@PostMapping(value="/stock",consumes = "multipart/form-data")
	public String addStock(@RequestHeader("Authorization") String authorization, RequestStockDto stock);

	/**
	 * Delete all stocks.
	 * 
	 * @param authorization
	 */
	@DeleteMapping(value = "/stock")
	public void deleteAllStocks(@RequestHeader("Authorization") String authorization);

}
