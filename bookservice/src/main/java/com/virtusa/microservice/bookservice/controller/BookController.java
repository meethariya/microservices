package com.virtusa.microservice.bookservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.virtusa.microservice.bookservice.dto.RequestBookDto;
import com.virtusa.microservice.bookservice.dto.ResponseBookDto;
import com.virtusa.microservice.bookservice.service.BookService;

/**
 * Controller for Book API
 * 
 * @author MEETKIRTIBHAI
 * @since 03-Sep-2023
 */
@RestController
@RequestMapping("/book")
public class BookController {

	@Autowired
	BookService bookService;

	/**
	 * Add a book
	 * 
	 * @param book
	 * @return ID of the book
	 */
	@PostMapping
	public ResponseEntity<String> addBook(@ModelAttribute RequestBookDto book, Authentication auth) {
		return new ResponseEntity<>(bookService.addBook(book, auth.getName(), auth.getCredentials().toString()), HttpStatus.CREATED);
	}

	/**
	 * Get all books
	 * 
	 * @return List of books
	 */
	@GetMapping
	public ResponseEntity<List<ResponseBookDto>> getAllBooks() {
		return new ResponseEntity<>(bookService.getAllBooks(), HttpStatus.OK);
	}

	/**
	 * Get book by Id.
	 * 
	 * @param id
	 * @return book
	 */
	@GetMapping("/{id}")
	public ResponseEntity<ResponseBookDto> getBookById(@PathVariable("id") String id) {
		return new ResponseEntity<>(bookService.getBookById(id), HttpStatus.OK);
	}

	/**
	 * Update book by id
	 * 
	 * @param id
	 * @param book
	 * @return updated book
	 */
	@PutMapping("/{id}")
	public ResponseEntity<ResponseBookDto> updateBook(@PathVariable("id") String id,
			@ModelAttribute RequestBookDto book) {
		return new ResponseEntity<>(bookService.updateBook(id, book), HttpStatus.ACCEPTED);
	}

	/**
	 * Delete book by ID.
	 * 
	 * @param id
	 */
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void deleteBookById(@PathVariable("id") String id, Authentication auth) {
		bookService.deleteBookById(id, auth.getName(), auth.getCredentials().toString());
	}

	/**
	 * Delete all books.
	 */
	@DeleteMapping
	@ResponseStatus(HttpStatus.OK)
	public void deleteAllBooks(Authentication auth) {
		bookService.deleteAllBooks(auth.getName(), auth.getCredentials().toString());
	}
}
