package com.virtusa.microservice.bookservice.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

/**
 * @author MEETKIRTIBHAI
 * @since 03-Sep-2023
 */
@Document
@Data
public class Category {
	@Id
	private String id;
	@Indexed(unique = true)
	private String name;
}
