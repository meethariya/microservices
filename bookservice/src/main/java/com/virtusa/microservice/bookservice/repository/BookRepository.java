package com.virtusa.microservice.bookservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.virtusa.microservice.bookservice.model.Book;

/**
 * @author MEETKIRTIBHAI
 * @since 03-Sep-2023
 */
public interface BookRepository extends MongoRepository<Book, String> {

}
