package com.virtusa.microservice.bookservice.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.virtusa.microservice.bookservice.model.Category;

/**
 * @author MEETKIRTIBHAI
 * @since 03-Sep-2023
 */
public interface CategoryRepository extends MongoRepository<Category, String> {
	/**
	 * Find Category by name.
	 * 
	 * @param name
	 * @return Optional of category
	 */
	public Optional<Category> findByName(String name);
}
