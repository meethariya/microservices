package com.virtusa.microservice.bookservice.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.virtusa.microservice.bookservice.exception.BookNotFoundException;
import com.virtusa.microservice.bookservice.exception.CategoryNotFoundException;

/**
 * Handles all Exceptions.
 * 
 * @author MEETKIRTIBHAI
 * @since 03-Sep-2023
 */
@RestControllerAdvice
public class ExceptionHandlerController {

	/**
	 * Handles error for BookNotFoundException
	 * @param e
	 * @return Error Message response
	 */
	@ExceptionHandler(BookNotFoundException.class)
	public ResponseEntity<String> handleBookNotFoundException(BookNotFoundException e){
		return new ResponseEntity<>(e.getMessage(),HttpStatus.NOT_FOUND);
	}
	
	/**
	 * Handles error for CategoryNotFoundException
	 * @param e
	 * @return Error Message response
	 */
	@ExceptionHandler(CategoryNotFoundException.class)
	public ResponseEntity<String> handleCategoryNotFoundException(CategoryNotFoundException e){
		return new ResponseEntity<>(e.getMessage(),HttpStatus.NOT_FOUND);
	}
}
