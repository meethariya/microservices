package com.virtusa.microservice.bookservice;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@SpringBootTest
@AutoConfigureMockMvc
class BookserviceApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@Test
	void getAllBook() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/book")).andExpect(status().isOk());
	}

	@Test
	void getBookById() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/book/abcd")).andExpect(status().isNotFound());
	}

}
